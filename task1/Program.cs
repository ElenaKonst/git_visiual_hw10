﻿using System;
//ввод-вывод

namespace task1
{
    internal class Program
    {
        static void Main()
        {
            // вывод информации на экран
            Console.WriteLine("Привет, мир!");
            int a = 2, b = 10;
            Console.WriteLine(a);
            Console.WriteLine(a + b * 2 - 3 * a);
            Console.WriteLine("Slovo {1} {0}", a, b);
            Console.WriteLine($"Вывод с переменной {a}");
            // Ввод информации с клавиатуры
            string name = Console.ReadLine();

            Console.WriteLine($"Привет, {name}");

            // задержка экрана
            Console.ReadKey();

        }
    }
}
